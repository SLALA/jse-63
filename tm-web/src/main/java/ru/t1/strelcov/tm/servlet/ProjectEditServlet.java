package ru.t1.strelcov.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {
    @SneakyThrows
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        req.setAttribute("project", ProjectRepository.getInstance().findById(req.getParameter("id")));
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @SneakyThrows
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) {
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final Status status = Status.valueOf(req.getParameter("status"));
        final String dateStartString = req.getParameter("dateStart");
        Date dateStart = null;
        if (!dateStartString.isEmpty())
            dateStart = new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter("dateStart"));
        final Project project = ProjectRepository.getInstance().findById(req.getParameter("id"));
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);
        project.setDateStart(dateStart);
        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }
}