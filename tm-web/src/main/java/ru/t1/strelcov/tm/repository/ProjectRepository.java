package ru.t1.strelcov.tm.repository;

import lombok.NoArgsConstructor;
import ru.t1.strelcov.tm.model.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public class ProjectRepository {

    private static final Map<String, Project> projects = new LinkedHashMap<>();

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    {
        add("Project 1", "Project created.");
    }

    public void add(final String name) {
        final Project project = new Project(name);
        projects.put(project.getId(), project);
    }

    public void add(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.put(project.getId(), project);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

}