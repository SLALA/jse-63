package ru.t1.strelcov.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.strelcov.tm.repository.TaskRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task/remove/*")
public class TaskRemoveServlet extends HttpServlet {
    @SneakyThrows
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        TaskRepository.getInstance().removeById(req.getParameter("id"));
        resp.sendRedirect("/tasks");
    }
}