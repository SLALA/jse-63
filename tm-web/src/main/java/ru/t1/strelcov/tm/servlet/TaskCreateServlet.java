package ru.t1.strelcov.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.strelcov.tm.repository.TaskRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task/create/*")
public class TaskCreateServlet extends HttpServlet {
    @SneakyThrows
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) {
        int number = TaskRepository.getInstance().findAll().size() + 1;
        TaskRepository.getInstance().add("Task " + number);
        resp.sendRedirect("/tasks");
    }
}