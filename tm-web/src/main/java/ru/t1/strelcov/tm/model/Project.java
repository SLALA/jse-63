package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

import static ru.t1.strelcov.tm.enumerated.Status.NOT_STARTED;

@Getter
@Setter
public class Project {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateStart;

    public Project(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    public Project(@NotNull String name) {
        this.name = name;
    }

}
