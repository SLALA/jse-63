package ru.t1.strelcov.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.strelcov.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/project/create/*")
public class ProjectCreateServlet extends HttpServlet {
    @SneakyThrows
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) {
        int number = ProjectRepository.getInstance().findAll().size() + 1;
        ProjectRepository.getInstance().add("Project " + number);
        resp.sendRedirect("/projects");
    }
}