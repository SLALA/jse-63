package ru.t1.strelcov.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.repository.ProjectRepository;
import ru.t1.strelcov.tm.repository.TaskRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {
    @SneakyThrows
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        req.setAttribute("task", TaskRepository.getInstance().findById(req.getParameter("id")));
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @SneakyThrows
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) {
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final Status status = Status.valueOf(req.getParameter("status"));
        final String dateStartString = req.getParameter("dateStart");
        final String projectId = req.getParameter("projectId");
        Date dateStart = null;
        if (!dateStartString.isEmpty())
            dateStart = new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter("dateStart"));
        final Task task = TaskRepository.getInstance().findById(req.getParameter("id"));
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);
        task.setDateStart(dateStart);
        task.setProjectId(projectId);
        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }
}