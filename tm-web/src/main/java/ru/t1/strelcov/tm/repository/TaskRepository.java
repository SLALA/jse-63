package ru.t1.strelcov.tm.repository;

import lombok.NoArgsConstructor;
import ru.t1.strelcov.tm.model.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public class TaskRepository {

    private final Map<String, Task> Tasks = new LinkedHashMap<>();

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    {
        add("Task 1", "Task created.");
    }

    public void add(final String name) {
        final Task Task = new Task(name);
        Tasks.put(Task.getId(), Task);
    }

    public void add(final String name, final String description) {
        final Task Task = new Task(name, description);
        Tasks.put(Task.getId(), Task);
    }

    public void removeById(final String id) {
        Tasks.remove(id);
    }

    public List<Task> findAll() {
        return new ArrayList<>(Tasks.values());
    }

    public Task findById(final String id) {
        return Tasks.get(id);
    }

    public void setProjectId(final String id, final String projectId) {
        Tasks.get(id).setProjectId(projectId);
    }

    public void save(final Task Task) {
        Tasks.put(Task.getId(), Task);
    }

}