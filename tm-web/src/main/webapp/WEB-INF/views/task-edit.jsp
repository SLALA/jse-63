<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Edit task</h1>

<form action="/task/edit?id=${task.id}" method="post">
    <p>
    <div>Id:</div>
    <input type="text" readonly disabled name="id" value="${task.id}"/>
    </p>
    <p>
    <div>Name:</div>
    <div><input type="text" name="name" value="${task.name}"/></div>
    </p>
    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${task.description}"/></div>
    </p>
    <p>
    <div>Status:</div>
    <select name="status">
        <c:forEach var="status" items="${statuses}">
            <option
                    <c:if test="${task.status == status}">selected="selected"</c:if> value="${status}">
                    ${status.displayName}
            </option>
        </c:forEach>
    </select>
    </p>
    <p>
    <div>Date Start:</div>
    <div>
        <input type="date" name="dateStart" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateStart}" />"/>
    </div>
    </p>
    <p>
    <div>Project:</div>
    <select name="projectId">
        <option value="">--</option>
        <c:forEach var="project" items="${projects}">
            <option
                    <c:if test="${project.id == task.projectId}">selected="selected"</c:if> value="${project.id}">
                    ${project.name}
            </option>
        </c:forEach>
    </select>
    </p>
    <button type="submit">Save</button>
</form>
<jsp:include page="../include/_footer.jsp"/>