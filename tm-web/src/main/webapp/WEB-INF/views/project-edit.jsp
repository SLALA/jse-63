<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Edit project</h1>

<form action="/project/edit?id=${project.id}" method="post">
    <p>
    <div>Id:</div>
    <input type="text" readonly disabled name="id" value="${project.id}"/>
    </p>
    <p>
    <div>Name:</div>
    <div><input type="text" name="name" value="${project.name}"/></div>
    </p>
    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${project.description}"/></div>
    </p>
    <p>
    <div>Status:</div>
    <select name="status">
        <c:forEach var="status" items="${statuses}">
            <option
                    <c:if test="${project.status == status}">selected="selected"</c:if> value="${status}">
                    ${status.displayName}
            </option>
        </c:forEach>
    </select>
    </p>
    <p>
    <div>Date Start:</div>
    <div>
        <input type="date" name="dateStart"
               value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateStart}" />"/>
    </div>
    </p>
    <button type="submit">Save</button>
</form>
<jsp:include page="../include/_footer.jsp"/>