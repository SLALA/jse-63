package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;

@Component
public final class ExitListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Exit program.";
    }

    @Override
    @EventListener(condition = "@exitListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

}
