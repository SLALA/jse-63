package ru.t1.strelcov.tm.listener;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.component.MessageExecutor;
import ru.t1.strelcov.tm.enumerated.EntityEventType;

import javax.persistence.*;

public final class EntityListener {

    @NotNull
    private static final MessageExecutor MESSAGE_EXECUTOR = new MessageExecutor();

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        MESSAGE_EXECUTOR.sendMessage(entity, EntityEventType.POST_LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        MESSAGE_EXECUTOR.sendMessage(entity, EntityEventType.PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        MESSAGE_EXECUTOR.sendMessage(entity, EntityEventType.POST_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        MESSAGE_EXECUTOR.sendMessage(entity, EntityEventType.PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        MESSAGE_EXECUTOR.sendMessage(entity, EntityEventType.POST_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        MESSAGE_EXECUTOR.sendMessage(entity, EntityEventType.PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        MESSAGE_EXECUTOR.sendMessage(entity, EntityEventType.POST_UPDATE);
    }

}
