package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.api.service.dto.ITaskDTOService;
import ru.t1.strelcov.tm.dto.model.SessionDTO;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.Status;

import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint")
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Autowired
    @NotNull
    private ITaskDTOService taskDTOService;

    @NotNull
    @Override
    public TaskListResponse listTask(@NotNull TaskListRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskListResponse(taskDTOService.findAll(session.getUserId()));
    }

    @NotNull
    @Override
    public TaskListSortedResponse listSortedTask(@NotNull TaskListSortedRequest request) {
        @NotNull final SessionDTO session = check(request);
        final String sort = request.getSort();
        return new TaskListSortedResponse(taskDTOService.findAll(session.getUserId(), sort));
    }

    @NotNull
    @Override
    public TaskListByProjectIdResponse listByProjectIdTask(@NotNull TaskListByProjectIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskListByProjectIdResponse(taskDTOService.findAllTasksByProjectId(session.getUserId(), request.getProjectId()));
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeStatusByIdTask(@NotNull TaskChangeStatusByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskChangeStatusByIdResponse(taskDTOService.changeStatusById(session.getUserId(), request.getId(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public TaskChangeStatusByNameResponse changeStatusByNameTask(@NotNull TaskChangeStatusByNameRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskChangeStatusByNameResponse(taskDTOService.changeStatusByName(session.getUserId(), request.getName(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        @NotNull final SessionDTO session = check(request);
        taskDTOService.clear(session.getUserId());
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskCreateResponse(taskDTOService.add(session.getUserId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public TaskFindByIdResponse findByIdTask(@NotNull TaskFindByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskFindByIdResponse(taskDTOService.findById(session.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public TaskFindByNameResponse findByNameTask(@NotNull TaskFindByNameRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskFindByNameResponse(taskDTOService.findByName(session.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeByIdTask(@NotNull TaskRemoveByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskRemoveByIdResponse(taskDTOService.removeById(session.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public TaskRemoveByNameResponse removeByNameTask(@NotNull TaskRemoveByNameRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskRemoveByNameResponse(taskDTOService.removeByName(session.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskBindToProjectResponse(taskDTOService.bindTaskToProject(session.getUserId(), request.getTaskId(), request.getProjectId()));
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskUnbindFromProjectResponse(taskDTOService.unbindTaskFromProject(session.getUserId(), request.getTaskId()));
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateByIdTask(@NotNull TaskUpdateByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskUpdateByIdResponse(taskDTOService.updateById(session.getUserId(), request.getId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public TaskUpdateByNameResponse updateByNameTask(@NotNull TaskUpdateByNameRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new TaskUpdateByNameResponse(taskDTOService.updateByName(session.getUserId(), request.getOldName(), request.getName(), request.getDescription()));
    }

}
